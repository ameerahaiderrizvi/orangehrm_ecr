terraform {
  backend "s3" {
    bucket  = "my-terraform-state-bucket-cicd"
    key     = "terraform/state/terraform.tfstate"
    region  = "us-east-1"
    encrypt = true
  }
}

provider "aws" {
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  region     = var.aws_region
}

module "vpc" {
  source = "./modules/vpc"
  vpc_cidr = var.vpc_cidr
  name_prefix = var.name_prefix
  public_subnets = var.public_subnets
  availability_zones = var.availability_zones
}

module "sg" {
  source = "./modules/sg"
  vpc_id = module.vpc.vpc_id
  name_prefix = var.name_prefix
}

//Database Server User Data
data "template_file" "db_userdata" {
  template = file("./db-userdata.sh")

  vars = {
    db_name = "mydb"
    db_username = "ameera"
    db_password = "12345"
  }
}

//Database Server
module "servers" {
  source = "./modules/servers"
  ami_id = var.ami_id
  key_name = var.key_name
  private_subnet_id = module.vpc.public_subnets_ids[0]
  db_security_group_id = module.sg.app_SG_id

  db_user_data   = data.template_file.db_userdata.rendered
  name_prefix = var.name_prefix
}

module "ecr" {
  source = "./modules/ecr"
  security_group_id    = module.sg.app_SG_id
  public_subnets_ids = module.vpc.public_subnets_ids
}


