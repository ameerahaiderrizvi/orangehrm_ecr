variable "security_group_id" {
  description = "The ID of the security group for the ECS service"
}

variable "public_subnets_ids" {
  description = "The IDs of the public subnets"
  type        = list(string)
}
