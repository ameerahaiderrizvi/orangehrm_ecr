output "db_host" {
  description = "The host of the database server"
  value       = aws_instance.database_server.private_ip
}
